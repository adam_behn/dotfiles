#!/usr/bin/env bash
xcode-select --install

# === Section: brew ===
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
brew upgrade

# == GNU Tools
brew install coreutils
brew install inetutils
brew install moreutils
brew install gnu-sed --with-default-names
brew install grep
brew install ssh-copy-id

# == Bash
brew install bash
brew install bash-git-prompt
brew install bash-completion
brew install docker-completion
brew install vagrant-completion

# == Utilities
brew install ansiweather
brew install autojump
brew install doctl
brew install fd
brew install mas
brew install mtr
brew install python3
brew install ripgrep
brew install rsync
brew install tree
brew install watch
brew install whatismyip
brew install wifi-password

# == Git
brew install git
brew install git-extras

# == GPG
# https://gist.github.com/bmhatfield/cc21ec0a3a2df963bffa3c1f884b676b#gistcomment-2165971
brew install gnupg
brew install pinentry-mac
mkdir ~/.gnupg
echo "pinentry-program /usr/local/bin/pinentry-mac" > ~/.gnupg/gpg-agent.conf


# == Cask
brew install balenaetcher
brew install brooklyn
brew install coconutbattery
brew install dbeaver-community
brew install decrediton
brew install docker
brew install exodus
brew install firefox
brew install forklift
brew install gitup
brew install keybase
brew install mtmr
brew install signal
brew install slack
brew install spotify
brew install steam
brew install telegram
brew install transmission
brew install vagrant
brew install virtualbox
brew install virtualbox-extension-pack
brew install viscosity

brew cleanup

# Switch to using brew-installed bash as default shell.
if ! fgrep -q '/usr/local/bin/bash' /etc/shells; then
  echo '/usr/local/bin/bash' | sudo tee -a /etc/shells;
  chsh -s /usr/local/bin/bash;
fi;
